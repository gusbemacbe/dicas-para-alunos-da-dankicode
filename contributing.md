---
layout: default
title: "Contribuição"
description: Realizar as contribuições para a Dankicode
permalink: /contributing.html
---

# Contribuição

- O contribuidor pode ser aluno iniciante que teve uma ideia, aluno veterano da Dankicode ou membro da Dankicode.
- Caso queira adicionar as dicas faltadas que quer passar para os alunos iniciantes se tornarem bastante autossuficientes, eficazes e produtivos. 
- Caso o aluno iniciante queira passar-nos a ideia como se fosse uma dica, analisaremos se é nos importante.