---
layout: default
title: "Captura de vídeo"
description: pages.captura_de_video.description
permalink: /captura-de-vídeo.html
---

{% if site.lang == "pt_PT" %}
  {% capture link1 %}{{ site.baseurl_root }}{{ page.url }}{% endcapture %}
  Você está em português europeu, e caso seja de Brasil, escolha um dos **idiomas:** <a href="{{ link1 }}" alt="Português brasileiro" name="Português brasileiro" title="Português brasileiro">{% t global.pt_br %}</a>
{% elsif site.lang == "pt_BR" %}
  {% capture link2 %}{{ site.baseurl_root }}/pt_PT{{ page.url }}{% endcapture %}
  Estás em português brasileiro, e caso sejas de Portugal, escolhe um dos **idiomas:** <a href="{{ link2 }}" alt="Português europeu" name="Português europeu" title="Português europeu">{% t global.pt_pt %}</a>
{% endif %}

<h1>Gravar um vídeo de {% t global.palavras.tela %} do computador</h1>

> É importante observar que alguns vídeos gravados do {% t t global.palavras.celular %} não são eficientes e legíveis para a compreensão das pessoas e não são claras para as explanações. Há pessoas que entendem os vídeos, mas ainda se incomodam e podem ficar frustradas com os vídeos gravados dos {% t global.palavras.celulares %}, por isto, elas preferem os gravados do computador.

Por isso, recomendamos que {% t global.palavras.você %} grave{% t global.palavras.verbo.s %} uma captura cheia ou selecionada de vídeo a partir do computador. Preparamos os minitutorais divididos para {% t global.palavras.usuários %} de cada sistema {% t global.palavras.operacional %}.

<h2>Conteúdo</h2>

- [Windows 7, 8 e 10](#windows-7-8-e-10)
  - [OBS Studio](#obs-studio)
- [Linux](#linux)
  - [OBS Studio](#obs-studio-1)
    - [Universal independentemente da distribuição](#universal-independentemente-da-distribuição)
    - [Alpine Linux](#alpine-linux)
    - [Arch Linux e seus derivados](#arch-linux-e-seus-derivados)
    - [CentOS, Fedora e RedHat](#centos-fedora-e-redhat)
    - [Debian, Ubuntu e seus derivados](#debian-ubuntu-e-seus-derivados)
    - [Gentoo e seus derivados](#gentoo-e-seus-derivados)
    - [openSUSE e seus derivados](#opensuse-e-seus-derivados)
    - [Solus](#solus)
    - [Void Linux](#void-linux)
    - [Configurar o OBS Studio](#configurar-o-obs-studio)
  - [Peek](#peek)
    - [Universal independentemente da distribuição](#universal-independentemente-da-distribuição-1)
    - [Alpine Linux](#alpine-linux-1)
    - [Arch Linux e seus derivados](#arch-linux-e-seus-derivados-1)
    - [CentOS, Fedora e RedHat](#centos-fedora-e-redhat-1)
    - [Debian, Ubuntu e seus derivados](#debian-ubuntu-e-seus-derivados-1)
    - [Gentoo e seus derivados](#gentoo-e-seus-derivados-1)
    - [openSUSE e seus derivados](#opensuse-e-seus-derivados-1)
    - [Solus](#solus-1)
    - [Void Linux](#void-linux-1)
    - [Configurar o Peek](#configurar-o-peek)
- [FreeBSD](#freebsd)
  - [OBS Studio](#obs-studio-2)

## Windows 7, 8 e 10

Não há aplicativo nativo de gravar a captura de {% t global.palavras.tela %}, por isso, passaremos o aplicativo fortemente recomendado.

<blockquote class="vermelho">
  <div>
  No Windows 10, há uma combinação da tecla <kbd><i class="fab fa-windows"></i></kbd> + <kbd>G</kbd>, que abre o aplicativo {% t global.palavras.aspaaberta %}Xbox Game Bar{% t global.palavras.aspafechada %}, que oferece a opção de gravar um vídeo de {% t global.palavras.tela %}, mas infelizmente esta opção é exclusiva para os <i>hardcore gamers</i> que <b>estiverem {% t global.palavras.gerúndio.jogar %}</b>. Não funciona para os desenvolvedores que <b>não estiverem {% t global.palavras.gerúndio.jogar %}</b>.
  </div>
</blockquote>


### OBS Studio

É um *software* gratuito e de código aberto para gravação de vídeo e transmissão ao vivo. 

1. <span class="capitalização">{% t global.palavras.verbo.clique %}</span> no [aplicativo para {% t global.palavras.verbo.baixar %} aqui](https://obsproject.com/pt-br/download);
2. <span class="capitalização">{% t global.palavras.verbo.selecione %}</span> {% t global.símbolo.aspaaberta %}Windows{% t global.palavras.aspafechada %} e receberá{% t global.palavras.verbo.s %} o diálogo de {% t global.palavras.transferência %};
3. <span class="capitalização">{% t global.palavras.verbo.instale %}</span>-o;
4. Agora {% t global.palavras.verbo.abra %}-o. Receberá{% t global.palavras.verbo.s %} o diálogo de assistente de configuração:
   1. <span class="capitalização">{% t global.palavras.verbo.selecione %}</span> {% t global.símbolo.aspaaberta %}Sim{% t global.símbolo.aspafechada %};
   2. <span class="capitalização">{% t global.palavras.verbo.selecione %}</span> {% t global.símbolo.aspaaberta %}Otimizar somente para gravação, eu não farei transmissão{% t global.símbolo.aspafechada %};
   3. Na Configuração de Vídeo, {% t global.palavras.verbo.deixe %} as configurações desta mesma forma;
   4. <span class="capitalização">{% t global.palavras.verbo.deixe %}</span> o teste automático de resultados finais. Então {% t global.palavras.verbo.clique %} {% t global.símbolo.aspaaberta %}Aplicar configurações{% t global.palavras.aspafechada %}.
5. Na figura 1, {% t global.palavras.verbo.selecione %} o símbolo {% t global.símbolo.aspaaberta%}+{% t global.símbolo.aspafechada %} para adicionar uma nova fonte:

   <figure>
     <figcaption>Figura 1</figcaption>
     <img alt="Adicionar uma fonte no OBS" name="Adicionar uma fonte no OBS" title="Adicionar uma fonte no OBS" src="assets/imagens/capturas/10-captura-obs.png"/>
   </figure>

6. Na figura 2, primeiramente {% t global.palavras.verbo.selecione %} a janela de editor de texto para gravar, então o símbolo 
{% t global.símbolo.aspaaberta%}Captura de Janela{% t global.símbolo.aspafechada %}:

   <figure>
     <figcaption>Figura 2</figcaption>
     <img alt="Selecionar a captura de janela no OBS" name="Selecionar a captura de janela no OBS" title="Selecionar a captura de janela no OBS" src="assets/imagens/capturas/12-captura-obs.png"/> 
   </figure>

   Fica assim:

   <img alt="Exemplo de previsão no OBS" name="Exemplo de previsão no OBS" title="Exemplo de previsão no OBS" src="assets/imagens/capturas/13-captura-obs.png"/>

7. <span class="capitalização">{% t global.palavras.verbo.minimize %}</span> o aplicativo OBS e {% t global.palavras.verbo.selecione %} o ícone do aplicativo na lista de ícones escondidos para iniciar a transmissão. Voilà. 😊

    <img alt="Lista de ícones escondidos" name="Lista de ícones escondidos" title="Lista de ícones escondidos" src="assets/imagens/capturas/14-captura-obs.png"/>

## Linux

### OBS Studio

#### Universal independentemente da distribuição

**Aplica-se:** CentOS, Mageia, Porteus, RedHat e Slackware.

1. <span class="capitalização">{% t global.palavras.verbo.observe %}</span> que se não {% t global.palavras.verbo.tiver %} Flatpak ou Snap, é necessário instalá-los.

   <dl>
     <dt>Flatpak (recomendado)</dt>
     <dd><span class="capitalização">{% t global.palavras.verbo.abra %}</span> o terminal:

     ```zsh
     flatpak install flathub com.obsproject.Studio --user
     ```

     </dd>

     <dt>Snapcraft (não recomendado)</dt>
     <dd><span class="capitalização">{% t global.palavras.verbo.abra %}</span> o terminal:

     ```zsh
     sudo snap install obs-studio --edge
     ```

     <p>Então para {% t global.palavras.verbo.acessar %} o conteúdo do armazenamento externo, {% t global.palavras.verbo.conecte %}-o manualmente ao plugue `mídia removível`:</p>
     
     ```zsh
     sudo snap connect obs-studio:removable-media
     ```

     </dd>
   </dl>

2. Agora {% t global.palavras.verbo.vá %} [configurar o OBS Studio](#configurar-o-obs-studio).

#### Alpine Linux

1. Está oficialmente no respoitório de Alpine, basta instalar:

   ```zsh
   apk add obs-studio
   ```

2. Agora {% t global.palavras.verbo.vá %} [configurar o OBS Studio](#configurar-o-obs-studio).

#### Arch Linux e seus derivados

<b>Derivados:</b> ArcoLinux, Endeavour e Manjaro

1. Já está no repositório do Arch Linux

   ```zsh
   sudo pacman -S obs-studio
   ```

2. Agora {% t global.palavras.verbo.vá %} [configurar o OBS Studio](#configurar-o-obs-studio).

#### CentOS, Fedora e RedHat

1. Apenas adicionar o repositório RPM Fusion e logo instalar o poacte:

   ```zsh
   $  sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
   $  sudo dnf install obs-studio
   ```

2. Agora {% t global.palavras.verbo.vá %} [configurar o OBS Studio](#configurar-o-obs-studio).

#### Debian, Ubuntu e seus derivados

1. Primeiro adicionar o repositório se for Ubuntu, e então atualizar e instalar:

   ```zsh
   # Apenas para Ubuntu
   $ sudo add-apt-repository ppa:obsproject/obs-studio

   $ sudo apt update
   $ sudo apt install ffmpeg
   $ sudo apt install obs-studio
   ```

2. Agora {% t global.palavras.verbo.vá %} [configurar o OBS Studio](#configurar-o-obs-studio).

#### Gentoo e seus derivados

**Aplica-se:** Bentōō e Funtoo

1. A compilação e instalação são muito simples:

   ```zsh
   sudo emerge media-video/peek
   ```

2. Agora {% t global.palavras.verbo.vá %} [configurar o OBS Studio](#configurar-o-obs-studio).

#### openSUSE e seus derivados

1. O repositório Packman contém o pacote `obs-studio`, pois requer a versão mais completa do FFmpeg, que está no Packman por razões legais. Se {% t global.palavras.você %} ainda não possui{% t global.palavras.verbo.s %} o repositório Packman como mostrado abaixo.

   - Para openSUSE Tumbleweed:

   ```zsh
   sudo zypper ar --refresh --priority 90 http://packman.inode.at/suse/openSUSE_Tumbleweed packman
   ```

   - Para openSUSE Leap 15.0:

   ```zsh
   sudo zypper ar --refresh --priority 90 http://packman.inode.at/suse/openSUSE_Leap_15.0 packman
   ```

   - Para openSUSE Leap 42.3:

   ```zsh
   sudo zypper ar --refresh http://packman.inode.at/suse/openSUSE_Leap_42.3 packman
   ```

2. Recomenda-se definir a prioridade do Packman como menor, para que tenha precedência sobre os repositórios da base (não é preciso no Tumbleweed conforme incluído no comando inicial):

   ```zsh
   sudo zypper mr --priority 90 packman
   ```

3. A versão Packman do FFmpeg deve ser usada para suporte total ao `codec`. Para garantir que todos os pacotes FFmpeg existentes sejam alternados para as versões Packman, {% t global.palavras.verbo.execute %} o seguinte antes de instalar o obs-studio.

   ```zsh
   sudo zypper dup --repo packman
   ```

4. Finalmente, instala o OBS Studio:

   ```zsh
   sudo zypper in obs-studio
   ```

5. Agora {% t global.palavras.verbo.vá %} [configurar o OBS Studio](#configurar-o-obs-studio).

#### Solus

1. Está oficialmente no repositório do Solus:

   ```zsh
   sudo eopkg install obs-studio
   ```

2. Agora {% t global.palavras.verbo.vá %}  [configurar o OBS Studio](#configurar-o-obs-studio).

#### Void Linux

1. Primeiro atualizar e então instalar:

   ```zsh
   $ sudo xbps-install -S
   $ sudo xbps-install obs
   ```

2. Agora {% t global.palavras.verbo.vá %}  [configurar o OBS Studio](#configurar-o-obs-studio).

#### Configurar o OBS Studio

1. Agora {% t global.palavras.verbo.abra %}-o. Receberá{% t global.palavras.verbo.s %} o diálogo de assistente de configuração:
   1. <span class="capitalização">{% t global.palavras.verbo.selecione %}</span> {% t global.símbolo.aspaaberta %}Sim{% t global.símbolo.aspafechada %};
   2. <span class="capitalização">{% t global.palavras.verbo.selecione %}</span> {% t global.símbolo.aspaaberta %}Otimizar somente para gravação, eu não farei transmissão{% t global.símbolo.aspafechada %};
   3. Na Configuração de Vídeo, {% t global.palavras.verbo.deixe %} as configurações desta mesma forma;
   4. <span class="capitalização">{% t global.palavras.verbo.deixe %}</span> o teste automático de resultados finais. Então {% t global.palavras.verbo.clique %} {% t global.símbolo.aspaaberta %}Aplicar configurações{% t global.símbolo.aspafechada %}.
2. Na figura 1, {% t global.palavras.verbo.selecione %} o símbolo {% t global.símbolo.aspaaberta%}+{% t global.símbolo.aspafechada %} para adicionar uma nova fonte:

   <figure>
     <figcaption>Figura 1</figcaption>
     <img alt="Adicionar uma fonte no OBS" name="Adicionar uma fonte no OBS" title="Adicionar uma fonte no OBS" src="assets/imagens/capturas/15-captura-obs.png"/>
   </figure>

3. Na figura 2, primeiramente {% t global.palavras.verbo.selecione %} a janela de editor de texto para gravar, então o símbolo 
{% t global.símbolo.aspaaberta%}Captura de Janela{% t global.símbolo.aspafechada %}:

   <figure>
     <figcaption>Figura 2</figcaption>
     <img alt="Selecionar a captura de janela no OBS" name="Selecionar a captura de janela no OBS" title="Selecionar a captura de janela no OBS" src="assets/imagens/capturas/16-captura-obs.png"/> 
   </figure>

4. <span class="capitalização">{% t global.palavras.verbo.selecione %}</span> um dos aplicativos, por exemplo, no caso de {% t global.símbolo.aspaaberta%}VSCode Inisders{% t global.símbolo.aspafechada %} para {% t global.símbolo.aspaaberta %}Captura de Janela{% t global.símbolo.aspafechada %} na figura 3:

   <figure>
     <figcaption>Figura 3</figcaption>
     <img alt="Selecionar um dos aplicativos para a captura de janela no OBS" name="Selecionar um dos aplicativos para a captura de janela no OBS" title="Selecionar um dos aplicativos para a captura de janela no OBS" src="assets/imagens/capturas/17-captura-obs.png"/> 
   </figure>

8. Não feche{% t global.palavras.verbo.s %} o aplicativo, apenas minimizar o aplicativo, pois ele está minimizado na lista de estados na barra de tarefas, então para iniciar e parar a gravação:

   <img alt="Iniciar a gravação no OBS minimizado" name="Iniciar a gravação no OBS minimizado" title="Iniciar a gravação no OBS minimizado" src="assets/imagens/capturas/18-captura-obs.png"/> 

Voilà!

### Peek

Peek é mais eficiente e simples, e permite gravar como um vídeo ou até como um GIF.

#### Universal independentemente da distribuição

**Aplica-se:** CentOS, Mageia, Porteus, RedHat e Slackware.

1. <span class="capitalização">{% t global.palavras.verbo.observe %}</span> que se não {% t global.palavras.verbo.tiver %} Flatpak ou Snap, é necessário instalá-los.

   <dl>
     <dt>AppImage (altamente recomendado)</dt>
     <dd>
      Pode{% t global.palavras.verbo.s %} pegar a <a href="https://github.com/phw/peek/releases/download/1.3.0/peek-1.3.0-0-x86_64.AppImage">ligação direta aqui</a>.
     </dd>

     <dt>Flatpak (recomendado)</dt>
     <dd><span class="capitalização">{% t global.palavras.verbo.abra %}</span> o terminal:
      
      ```zsh
      flatpak install flathub com.uploadedlobster.peek --user
      ```

     </dd>

     <dt>Snapcraft (não recomendado)</dt>
     <dd>
        O Peek não oferece mais suporte oficial aos pacotes da Snapcraft. <span class="capitalização">{% t global.palavras.verbo.considere %}</span> {% t global.palavras.us %}ar as versões AppImage ou Flatpak.
     </dd>
   </dl>

2. Agora {% t global.palavras.vá %} [configurar o Peek](#configurar-o-peek).

#### Alpine Linux

1. Está oficialmente no respoitório de Alpine, basta instalar:

   ```zsh
   apk add obs-studio
   ```
2. Agora {% t global.palavras.vá %} [configurar o Peek](#configurar-o-peek).

#### Arch Linux e seus derivados

<b>Derivados:</b> ArcoLinux, Endeavour e Manjaro

1. Já está no repositório oficial do Arch Linux

   ```zsh
   sudo pacman -S peek
   ```

2. Recomendamos que instale{% t global.palavras.verbo.s %} os três *plugins*:

   ```zsh
   sudo pacman -S gst-plugins-good gst-plugins-ugly gifski
   ```
3. Agora {% t global.palavras.vá %} [configurar o Peek](#configurar-o-peek).

#### CentOS, Fedora e RedHat

1. A instalação é simples, mas um pacote e um *plugin* também precisam ser instalados:

   ```zsh
   $ sudo dnf install peek
   $ sudo dnf install ffmpeg
   $ sudo dnf install gstreamer1-plugins-ugly
   ```
2. Agora {% t global.palavras.vá %} [configurar o Peek](#configurar-o-peek).

#### Debian, Ubuntu e seus derivados

1. Primeiro adicionar os extras se for ElementaryOS e o repositório se for ElementaryOS ou Ubuntu, e então atualizar e instalar:

   ```zsh
   # Apenas para ElementaryOS
   $ sudo apt install software-properties-common

   # Apenas para Ubuntu
   $ sudo add-apt-repository ppa:peek-developers/stable

   $ sudo apt update
   $ sudo apt install ffmpeg
   $ sudo apt install peek
   ```

2. Agora {% t global.palavras.vá %} [configurar o Peek](#configurar-o-peek).

#### Gentoo e seus derivados

**Aplica-se:** Bentōō e Funtoo

1. A compilação e instalação são muito simples:

   ```zsh
   sudo emerge media-video/obs-studio
   ```

2. Agora {% t global.palavras.vá %} [configurar o Peek](#configurar-o-peek).

#### openSUSE e seus derivados

1. Primeiramente adicionar o repositório, então atualizar e instalar:

     - Para openSUSE Tumbleweed:

     ```zsh
     zypper addrepo https://download.opensuse.org/repositories/openSUSE:Factory/standard/openSUSE:Factory.repo
     zypper refresh
     zypper install peek
     ```

     - Para openSUSE Leap 15.1:

     ```zsh
     $ zypper addrepo https://download.opensuse.org/repositories/openSUSE:Leap:15.1/standard/openSUSE:Leap:15.1.repo
     $ zypper refresh
     $ zypper install peek
     ```

     - Para openSUSE Leap 15.2:

     ```zsh
     $ zypper addrepo https://download.opensuse.org/repositories/openSUSE:Leap:15.2/standard/openSUSE:Leap:15.2.repo
     $ zypper refresh
     $ zypper install peek
     ```

2. Agora {% t global.palavras.vá %} [configurar o Peek](#configurar-o-peek).

#### Solus

1. Está oficialmente no repositório do Solus:

   ```zsh
   sudo eopkg it peek
   ```

2. Agora {% t global.palavras.vá %} [configurar o Peek](#configurar-o-peek).

#### Void Linux

1. Atualizar e instalar ao mesmo tempo:

   ```zsh
   sudo xbps-install -Su peek
   ```

2. Agora {% t global.palavras.vá %} [configurar o Peek](#configurar-o-peek).

#### Configurar o Peek

1. Após a instalação, {% t global.palavras.verbo.abra %} o Peek. Na figura 1, {% t global.palavras.verbo.observe %}</span> que a janela de Peek tem uma borda visível e um fundo transparente. A borda visível serve para dimensionar livremente a janela:

   <figure>
      <figcaption>Figura 1</figcaption>

      ![Exemplo de janela de Peek](assets/imagens/capturas/19-captura-peek.png)

   </figure>

2. Na figura 2, {% t global.palavras.verbo.clique %} no ícone de hambúrger à direita superior da janela de Peek e {% t global.palavras.verbo.selecione %} {% t global.símbolo.aspaaberta %}Preferências{% t global.símbolo.aspafechada %}. Agora, {% t global.palavras.verbo.marque %} {% t global.símbolo.aspaaberta %}Use o gifski para GIFs de alta qualidade{% t global.símbolo.aspafechada %} e {% t global.palavras.verbo.aumente %} a percentagem de qualidade do GIF:

   <figure>
      <figcaption>Figura 2</figcaption>

      ![Preferências de Peek](assets/imagens/capturas/20-captura-peek.png)

   </figure>

3. Vamos redimensionar a janela de Peek até ao tamanho da janela do navegador, por exemplo, {% t global.palavras.verbo.clique %} para iniciar o vídeo:

   <!-- blank line -->
   <figure class="video_container">
     <video controls="true" allowfullscreen="true" poster="assets/imagens/capturas/21-captura-peek.png">
       <source src="assets/vídeos/mp4/1-vídeo-peek.mp4" type="video/mp4">
       <source src="assets/vídeos/ogg/1-vídeo-peek.ogg" type="video/ogg">
       <source src="assets/vídeos/webm/1-vídeo-peek.webm" type="video/webm">
     </video>
   </figure>
   <!-- blank line -->

4. Agora pode{% t global.palavras.verbo.s %} clicar em {% t global.símbolo.aspaaberta %}Gravar como GIF{% t global.símbolo.aspafechada %} (ou pode{% t global.palavras.verbo.s %} escolher gravar como MP4 ou WebM ao selecionar o ícone de *seta para baixo*), logo contará 3 segundos para iniciar e para parar, basta clicar em {% t global.símbolo.aspaaberta %}Parar{% t global.símbolo.aspafechada %}, e receberá{% t global.palavras.verbo.s %} o diálogo para salvar o GIF ou o vídeo. Voilà! 😉

## FreeBSD

### OBS Studio

1. <span class="capitalização">{% t global.palavras.verbo.instale %}</span> simplesmente o aplicativo:

   ```zsh
   pkg install obs-studio
   ```

2. <span class="capitalização">{% t global.palavras.verbo.siga %}</span> as mesmas [configurações o OBS Studio no Linux](#configurar-o-obs-studio).