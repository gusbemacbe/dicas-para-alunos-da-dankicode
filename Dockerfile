FROM ubuntu:focal

ENV DEBIAN_FRONTEND="noninteractive"

# Actualização e instalação de aplicativos e pacotes
RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get -y install build-essential curl dh-autoreconf git locales lsb-release nano neofetch nodejs openssl procps ruby-dev ruby-execjs ruby2.7 ruby2.7-dev sudo zlib1g-dev zsh

# Limpeza
RUN apt-get autoremove -y \
   && apt-get clean -y \
ENV DEBIAN_FRONTEND=dialog

RUN locale-gen en_US
RUN update-locale LANG=en_US
RUN update-locale LC_ALL=en_US

RUN export LANG=en_US.UTF-8
RUN export LC_ALL=en_US.UTF-8

RUN echo 'Set disable_coredump false' >> /etc/sudo.conf

# Entrar no ZSH ao invés de Bash
ENV SHELL /bin/zsh

# Criar o novo utilizador 
RUN useradd -rm -d /home/gusbemacbe -s /bin/bash -g root -G sudo -u 1000 gusbemacbe -p "$(openssl passwd -1 gusbemacbe)"
RUN adduser gusbemacbe sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

RUN gem install jekyll bundler

# Entrar como utilizador
USER gusbemacbe
WORKDIR /home/gusbemacbe
WORKDIR /home/gusbemacbe/dicas-para-alunos-da-dankicode
COPY .  /home/gusbemacbe/dicas-para-alunos-da-dankicode