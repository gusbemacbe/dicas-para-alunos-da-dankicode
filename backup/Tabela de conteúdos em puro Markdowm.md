---
layout: default
title: pages.inicio.title
description: pages.inicio.description
permalink: /index.html
---

{% if site.lang == "pt_PT" %}
  {% capture link1 %}{{ site.baseurl_root }}{{ page.url }}{% endcapture %}
  Você está em português europeu, e caso seja de Brasil, escolha um dos **idiomas:** <a href="{{ link1 }}" alt="Português brasileiro" name="Português brasileiro" title="Português brasileiro">{% t global.pt_br %}</a>
{% elsif site.lang == "pt_BR" %}
  {% capture link2 %}{{ site.baseurl_root }}/pt_PT{{ page.url }}{% endcapture %}
  Estás em português brasileiro, e caso sejas de Portugal, escolhe um dos **idiomas:** <a href="{{ link2 }}" alt="Português europeu" name="Português europeu" title="Português europeu">{% t global.pt_pt %}</a>
{% endif %}

# {% t pages.inicio.title %}

Uma lista de {% t global.palavras.dicas %} de ouro para os alunos iniciantes da Dankicode

## Motivo

Por exemplo, as capturas de {% t global.palavras.tela %} que os alunos tiram a partir dos {% t global.palavras.celulares %}, o que dificultarão a compreensão das pessoas. Os alunos ficam confusos com as explicações do Professor Guilherme, principalmente no caso de como instalar Apache, MySQL e PHP, e de como carregar ou renderizar o projeto e não entendem porque o *site* não carrega ou não está corretamente renderizado. Também há outros casos. 

A lista de {% t global.palavras.dicas %} é preparada para os alunos iniciantes se tornarem eficazes e produtivos a fim de facilitar a compreensão dos alunos veteranos, dos instrutores e dos membros da Dankicode para que eles possam ajudar e resolver os problemas rapidamente. Além disto, ajudará os alunos a configurar e {% t global.palavras.us %}ar corretamente as quatro linguagens – CSS, HTML, Markdown e PHP para renderizar funcionalmente o seu projeto. Ainda haverá outras {% t global.palavras.dicas %}, como aplicar as regras da MVC (árvore ou raíz do diretório).

Por favor, primeiramente {% t global.palavras.verbo.selecione %} um dos tutorais e logo {% t global.palavras.verbo.escolha %} um dos sistemas {% t global.palavras.operacionais %}:

## Índex de conteúdo

1. <b>[Existe uma versão nativa de Telegram para computadores!](telegram)</b>
  - [Instalação binária no Windows, macOS e Linux](telegram#instalação-binária-no-windows-macos-e-linux)
  - [Instalação não-binária no Linux](telegram#instalação-não-binária-no-linux)
        - [Universal independentemente da distribuição](telegram#universal-independentemente-da-distribuição)
        - [Alpine Linux](telegram#alpine-linux)
        - [Arch Linux e seus derivados](telegram#arch-linux-e-seus-derivados)
        - [CentOS e RedHat](telegram#centos-e-redhat)
        - [Debian, Ubuntu e seus derivados:](telegram#debian-ubuntu-e-seus-derivados)
        - [Fedora e seus derivados](telegram#fedora-e-seus-derivados)
        - [Gentoo e seus derivados](telegram#gentoo-e-seus-derivados)
        - [openSUSE e seus derivados](telegram#opensuse-e-seus-derivados)
        - [Solus](telegram#solus)
        - [Void Linux](telegram#void-linux)
  - [Instalação em outros sistemas {% t global.palavras.operacionais %}](telegram#instalação-em-outros-sistemas)
        - [FreeBSD](telegram#freebsd)

2. <b>[Como tirar uma captura de {% t global.palavras.tela %} a partir do computador](captura-de-{% t global.palavras.tela %})</b>
  - [Windows 10](captura-de-tela#windows-10)
  - [Linux](captura-de-tela#linux)
        - [Universal independentemente da distribuição](captura-de-tela#universal-independentemente-da-distribuição)
        - [Alpine Linux](captura-de-tela#alpine-linux)
        - [Arch Linux e seus derivados](captura-de-tela#arch-linux-e-seus-derivados)
        - [CentOS, Fedora e RedHat](captura-de-tela#centos-fedora-e-redhat)
        - [Debian, Ubuntu e seus derivados](captura-de-tela#debian-ubuntu-e-seus-derivados)
        - [Gentoo e seus derivados](captura-de-tela#gentoo-e-seus-derivados)
        - [openSUSE e seus derivados](captura-de-tela#opensuse-e-seus-derivados)
        - [Solus](captura-de-tela#solus)
        - [Void Linux](captura-de-tela#void-linux)
  - [FreeBSD](captura-de-tela#freebsd)

3. <b>[Como gravar um vídeo da área de trabalho a partir do computador](captura-de-vídeo)</b>
  - [Windows 7, 8 e 10](captura-de-vídeo#windows-7-8-e-10)
        - [OBS Studio](captura-de-vídeo#obs-studio)
    - [Linux](captura-de-vídeo#linux)
        - [OBS Studio](captura-de-vídeo#obs-studio-1)
          - [Universal independentemente da distribuição](captura-de-vídeo#universal-independentemente-da-distribuição)
          - [Alpine Linux](captura-de-vídeo#alpine-linux)
          - [Arch Linux e seus derivados](captura-de-vídeo#arch-linux-e-seus-derivados)
          - [CentOS, Fedora e RedHat](captura-de-vídeo#centos-fedora-e-redhat)
          - [Debian, Ubuntu e seus derivados](captura-de-vídeo#debian-ubuntu-e-seus-derivados)
          - [Gentoo e seus derivados](captura-de-vídeo#gentoo-e-seus-derivados)
          - [openSUSE e seus derivados](captura-de-vídeo#opensuse-e-seus-derivados)
          - [Solus](captura-de-vídeo#solus)
          - [Void Linux](captura-de-vídeo#void-linux)
          - [Configurar o OBS Studio](captura-de-vídeo#configurar-o-obs-studio)
    - [FreeBSD](captura-de-vídeo#freebsd)
