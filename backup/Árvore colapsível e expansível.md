<details class="árvore-colapsível expansível">
  <summary class="árvore-colapsível-título"><a href="telegram">Existe uma versão nativa de Telegram para computadores!</a></summary>
  
  <details class="árvore-colapsível">
    <summary class="árvore-colapsível-título"><a href="telegram#instalação-binária-no-windows-macos-e-linux">Instalação binária no Windows, macOS e Linux</a></summary>
  </details>

  <details class="árvore-colapsível expansível">
    <summary class="árvore-colapsível-título"><a href="telegram#instalação-não-binária-no-linux">Instalação não-binária no Linux</a></summary>
      <div class="árvore-colapsível">
        <a class="árvore-colapsível-título" href="telegram#universal-independentemente-da-distribuição">Universal independentemente da distribuição</a>
        <a class="árvore-colapsível-título" href="telegram#alpine-linux">Alpine Linux</a>
        <a class="árvore-colapsível-título" href="telegram#arch-linux-e-seus-derivados">Arch Linux e seus derivados</a>
        <a class="árvore-colapsível-título" href="telegram#centos-e-redhat">CentOS e RedHat</a>
        <a class="árvore-colapsível-título" href="telegram#debian-ubuntu-e-seus-derivados">Debian, Ubuntu e seus derivados:</a>
        <a class="árvore-colapsível-título" href="telegram#fedora-e-seus-derivados">Fedora e seus derivados</a>
        <a class="árvore-colapsível-título" href="telegram#gentoo-e-seus-derivados">Gentoo e seus derivados</a>
        <a class="árvore-colapsível-título" href="telegram#opensuse-e-seus-derivados">openSUSE e seus derivados</a>
        <a class="árvore-colapsível-título" href="telegram#solus">Solus</a>
        <a class="árvore-colapsível-título" href="telegram#void-linux">Void Linux</a>
      </div>
  </details>

  <details class="árvore-colapsível expansível">
    <summary class="árvore-colapsível-título"><a href="telegram#instalação-em-outros-sistemas">Instalação em outros sistemas {% t global.palavras.operacionais %}</a></summary>
      <div class="árvore-colapsível">
        <a class="árvore-colapsível-título" href="telegram#freebsd">FreeBSD</a>
      </div>
  </details>
</details>

<!--  -->

<details class="árvore-colapsível expansível">
  <summary class="árvore-colapsível-título"><a href="captura-de-{% t global.palavras.tela %}">Como tirar uma captura de {% t global.palavras.tela %} a partir do computador</a></summary>

  <details class="árvore-colapsível">
    <summary class="árvore-colapsível-título"><a href="captura-de-{% t global.palavras.tela %}#windows-10">Windows 10</a></summary>
  </details>

  <details class="árvore-colapsível expansível">
    <summary class="árvore-colapsível-título"><a href="captura-de-{% t global.palavras.tela %}#linux">Linux</a></summary>
      <div class="árvore-colapsível">
        <a class="árvore-colapsível-título" href="captura-de-{% t global.palavras.tela %}#universal-independentemente-da-distribuição">Universal independentemente da distribuição</a>
        <a class="árvore-colapsível-título" href="captura-de-{% t global.palavras.tela %}#alpine-linux">Alpine Linux</a>
        <a class="árvore-colapsível-título" href="captura-de-{% t global.palavras.tela %}#arch-linux-e-seus-derivados">Arch Linux e seus derivados</a>
        <a class="árvore-colapsível-título" href="captura-de-{% t global.palavras.tela %}#centos-fedora-e-redhat">CentOS, Fedora e RedHat</a>
        <a class="árvore-colapsível-título" href="captura-de-{% t global.palavras.tela %}#debian-ubuntu-e-seus-derivados">Debian, Ubuntu e seus derivados</a>
        <a class="árvore-colapsível-título" href="captura-de-{% t global.palavras.tela %}#gentoo-e-seus-derivados">Gentoo e seus derivados</a>
        <a class="árvore-colapsível-título" href="captura-de-{% t global.palavras.tela %}#opensuse-e-seus-derivados">openSUSE e seus derivados</a>
        <a class="árvore-colapsível-título" href="captura-de-{% t global.palavras.tela %}#solus">Solus</a>
        <a class="árvore-colapsível-título" href="captura-de-{% t global.palavras.tela %}#void-linux">Void Linux</a>
      </div>
  </details>

  <details class="árvore-colapsível">
    <summary class="árvore-colapsível-título"><a href="captura-de-tela#freebsd">FreeBSD</a></summary>
  </details>
</details>

<!--  -->

 <details class="árvore-colapsível expansível">
  <summary class="árvore-colapsível-título"><a href="captura-de-vídeo">Como gravar um vídeo de {% t global.palavras.tela %} a partir do computador</a></summary>

  <details class="árvore-colapsível">
    <summary class="árvore-colapsível-título"><a href="captura-de-vídeo#windows-7-8-e-10">Windows 7, 8 e 10</a></summary>
      <div class="árvore-colapsível">
          <a class="árvore-colapsível-título" href="captura-de-vídeo#obs-studio)">OBS Studio</a>
      </div>
  </details>

  <details class="árvore-colapsível expansível">
    <summary class="árvore-colapsível-título"><a href="captura-de-vídeo#linux">Linux</a></summary>
    <details class="árvore-colpasível expansível">
      <summary class="árvore-colpasível-título"><a href="#obs-studio-1">OBS Studio</a></summary>
      <div class="árvore-colapsível">
        <a class="árvore-colapsível-título" href="captura-de-vídeo#universal-independentemente-da-distribuição">Universal independentemente da distribuição</a>
        <a class="árvore-colapsível-título" href="captura-de-vídeo#alpine-linux">Alpine Linux</a>
        <a class="árvore-colapsível-título" href="captura-de-vídeo#arch-linux-e-seus-derivados">Arch Linux e seus derivados</a>
        <a class="árvore-colapsível-título" href="captura-de-vídeo#centos-fedora-e-redhat">CentOS, Fedora e RedHat</a>
        <a class="árvore-colapsível-título" href="captura-de-vídeo#debian-ubuntu-e-seus-derivados">Debian, Ubuntu e seus derivados</a>
        <a class="árvore-colapsível-título" href="captura-de-vídeo#gentoo-e-seus-derivados">Gentoo e seus derivados</a>
        <a class="árvore-colapsível-título" href="captura-de-vídeo#opensuse-e-seus-derivados">openSUSE e seus derivados</a>
        <a class="árvore-colapsível-título" href="captura-de-vídeo#solus">Solus</a>
        <a class="árvore-colapsível-título" href="captura-de-vídeo#void-linux">Void Linux</a>
        <a class="árvore-colapsível-título" href="captura-de-vídeo#configurar-o-obs-studio">Configurar o OBS Studio</a>
      </div>
    </details>
  </details>

  <details class="árvore-colapsível expansível">
    <summary class="árvore-colapsível-título"><a href="captura-de-vídeo#freebsd">FreeBSD</a></summary>
  </details>