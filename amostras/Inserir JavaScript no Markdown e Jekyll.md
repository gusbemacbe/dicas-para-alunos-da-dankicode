---
layout: default
title: pages.inicio.title
description: pages.inicio.description
permalink: /index.html
customjs:
 - assets/js/árvore-colapsível.js
---

{% for js in page.customjs %}
<script type="text/javascript" src="{{ js }}"></script>
{% endfor %}