---
layout: default
title: "Amostras de notas"
description: "Amostras de notas derivadas dos sites Funtoo e Gentoo"
permalink: /amostras/amostras-de-notas.html
---

<link rel="stylesheet" type="text/css" href="{{ "/amostras/assets/css/notas.css" | prepend: site.baseurl_root }}"/>

<div class="note warningnote">
  <div class="note-head warningnote">
    <i class="fas fa-exclamation-triangle"></i>
    &nbsp;&nbsp;&nbsp;Aviso
  </div>
  <div class="note-body warningnote">
    <p>this will delete your .config</p>
  </div>
</div>

<div class="note important">
  <div class="note-head important"><i class="fas fa-exclamation"></i>&nbsp;&nbsp;&nbsp;Importante</div>
  <div class="note-body important">
    <p>Notice, when updating to newer kernel, a copied config file is for older version of kernel!</p>
  </div>
</div>

<div class="note">
  <div class="note-head"><i class="far fa-comment-alt"></i>&nbsp;&nbsp;&nbsp;<b>Nota</b></div>
  <div class="note-body">
    <p>Many pages on the wiki will tell you the kernel requirements for the application that they are about. Keep your eyes open for the blue background, white text sections of pages. Like on this one: <a href="/Uvesafb" title="Uvesafb"> uvesafb</a></p>
  </div>
</div>

<div class="note tip">
  <div class="note-head tip"><i class="far fa-hand-point-right"></i>&nbsp;&nbsp;&nbsp;Tip</div>
  <div class="note-body tip">
    <p>You can add -j&lt;number of processing cores + 1&gt; after make to build the kernel more quickly.</p>
  </div>
</div>

<div class="box-caption"><span class="label" style="margin-right: .5em; background-color: #4E9A06">KERNEL</span> <strong>General support</strong></div>
<pre class="captioned">
Device Drivers  --->
    Graphics support  ---> 
        <*/M> Direct Rendering Manager (XFree86 4.1.0 and higher DRI support) --->
        <*/M> ATI Radeon
        [*] Enable modesetting on radeon by default
        -*- Support for frame buffer devices  --->
            < >   ATI Radeon display support
</pre>

<div class="note codenote">
  <div class="note-head codenote">
    <i class="fas fa-code"></i>&nbsp;&nbsp;&nbsp;<code>~/.xinitrc</code>
  </div>
  <pre lang="{{{lang}}}">compton --opengl &amp;</pre>
</div>