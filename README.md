# Dicas para Alunos da Dankicode

<details>
  <summary class="span">Português brasileiro</summary>

  Uma lista de dicas de ouro, como tirar uma captura de tela ou gravar um vídeo a partir do computador, usar corretamente o CSS, o HTML e o PHP, etc., para alunos iniciantes da Dankicode que têm dificuldade de entender as explicações do Professor Guilherme Grillo.

  Visite o [*site* aqui](https://gusbemacbe.gitlab.io/dicas-para-alunos-da-dankicode/base.html). 
</details>

<details>
  <summary class="span">Português europeu</summary>

  Uma lista de recomendações de ouro, como tirar uma captura de ecrã ou gravar um vídeo a partir do computador, utilizar corretamente o CSS, o HTML e o PHP, etc., para alunos iniciantes da Dankicode que têm dificuldade de entender as explicações do Professor Guilherme Grillo.

  Visita o [*site* aqui](https://gusbemacbe.gitlab.io/dicas-para-alunos-da-dankicode/base.html). 
</details>