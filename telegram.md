---
layout: default
title: "Telegram nativo no computador"
description: "Os alunos iniciantes da Dankicode não estão cientes que existe uma versão nativa de Telegram para computadores"
permalink: /telegram.html
---

{% if site.lang == "pt_PT" %}
  {% capture link1 %}{{ site.baseurl_root }}{{ page.url }}{% endcapture %}
  Você está em português europeu, e caso seja de Brasil, escolha um dos **idiomas:** <a href="{{ link1 }}" alt="Português brasileiro" name="Português brasileiro" title="Português brasileiro">{% t global.pt_br %}</a>
{% elsif site.lang == "pt_BR" %}
  {% capture link2 %}{{ site.baseurl_root }}/pt_PT{{ page.url }}{% endcapture %}
  Estás em português brasileiro, e caso sejas de Portugal, escolhe um dos **idiomas:** <a href="{{ link2 }}" alt="Português europeu" name="Português europeu" title="Português europeu">{% t global.pt_pt %}</a>
{% endif %}

<h1><span style="text-transform: capitalize">{% t global.palavras.us %}ar</span> o Telegram nativo nos computadores</h1>

<h2>Conteúdo</h2>

- [Instalação binária no Windows, macOS e Linux](#instalação-binária-no-windows-macos-e-linux)
- [Instalação não-binária no Linux](#instalação-não-binária-no-linux)
  - [Universal independentemente da distribuição](#universal-independentemente-da-distribuição)
  - [Alpine Linux](#alpine-linux)
  - [Arch Linux e seus derivados](#arch-linux-e-seus-derivados)
  - [CentOS e RedHat](#centos-e-redhat)
  - [Debian, Ubuntu e seus derivados:](#debian-ubuntu-e-seus-derivados)
  - [Fedora e seus derivados](#fedora-e-seus-derivados)
  - [Gentoo e seus derivados](#gentoo-e-seus-derivados)
  - [openSUSE e seus derivados](#opensuse-e-seus-derivados)
  - [Solus](#solus)
  - [Void Linux](#void-linux)
- [Instalação em outros sistemas {% t global.palavras.operacionais %}](#instalação-em-outros-sistemas)
  - [FreeBSD](#freebsd)

A maioria dos alunos iniciantes da Dankicode tiraram a captura de {% t global.palavras.tela %} a partir do {% t global.palavras.celular %} e enviaram no Telegram no Android ou no iOS e não estão cientes que já existe uma versão nativa de Telegram para computadores.

É mais eficiente e produtivo enviar a captura de {% t global.palavras.tela %} a partir do computador [ver o tutorial de captura](captura) diretamente no Telegram no computador, o que facilitará a compreensão das pessoas e agilizará a resolução das pessoas nos problemas no seu projeto. 

## Instalação binária no Windows, macOS e Linux

Nós íamos postar as imagens de captura do *site*, mas achamos que não é necessário, porque preparamos as ligações (*links*) diretas, prontas e rápidas do instalador do repositório oficial dos desenvolvedores do Telegram no GitHub para computador.

<dl>
  <dt>
    Windows 7, 8 e 10
  </dt>
  <dd>
    <ul>
      <li><b>Windows 7 e 8:</b> <a href="https://github.com/telegramdesktop/tdesktop/releases/download/v1.9.19/tsetup.1.9.19.beta.exe">Ligação direta do instalador do Telegram Desktop</a></li>
      <li><b>Windows 10:</b>    <a href="https://www.microsoft.com/pt-br/p/telegram-desktop/9nztwsqntd0s?activetab=pivot:overviewtab">Telegram no Microsoft Store</a></li>
    </ul>
  </dd>

  <dt>
    Linux na arquitetura de 32 bits
  </dt>
  <dd><a href="https://github.com/telegramdesktop/tdesktop/releases/download/v1.9.19/tsetup32.1.9.19.beta.tar.xz">Ligação direta do binário do Telegram Desktop</a></dd>
  
  <dt>
    Linux na arquitetura de 64 bits
  </dt>
  <dd><a href="https://github.com/telegramdesktop/tdesktop/releases/download/v1.9.19/tsetup.1.9.19.beta.tar.xz">Ligação direta do binário do Telegram Desktop</a></dd>

  <p style="clear: both">Se {% t global.palavras.verbo.preferir %} instalar não-binariamente a partir do repositório oficial da distribuição, por favor, {% t global.palavras.verbo.veja %}  <a href="#instalação-não-binária-no-linux">Instalação não-binária no Linux</a>.</p>
  

  <dt>
    OS X 10.10 e 10.11
  </dt>
  <dd><a href="https://github.com/telegramdesktop/tdesktop/releases/download/v1.9.19/tsetup-osx.1.9.19.beta.dmg">Ligação direta do aplicativo nativo Telegram Desktop</a></dd>

  <dt>
    macOS 10.12 e acima
  </dt>
  <dd><a href="https://github.com/telegramdesktop/tdesktop/releases/download/v1.9.19/tsetup.1.9.19.beta.dmg">Ligação direta do aplicativo nativo Telegram Desktop</a></dd>
</dl> 

## Instalação não-binária no Linux

### Universal independentemente da distribuição

**Aplica-se:** CentOS, Mageia, Porteus, RedHat e Slackware.

<span class="capitalização">{% t global.palavras.verbo.observe %}</span>  que se não {% t global.palavras.verbo.tiver %}  Flatpak ou Snap, é necessário instalá-los.

<dl>
  <dt>Flatpak (recomendado)</dt>
  <dd><span class="capitalização">{% t global.palavras.verbo.abra %}</span> o terminal:

  ```zsh
  flatpak install flathub org.telegram.desktop --user
  ```

  </dd>

  <dt>Snapcraft (não recomendado)</dt>
  <dd><span class="capitalização">{% t global.palavras.verbo.abra %}</span>  o terminal:

  ```zsh
  sudo snap install telegram-sergiusens
  ```

  </dd>
</dl>

### Alpine Linux

```zsh
apk add telegram-desktop
```

### Arch Linux e seus derivados

<b>Derivados:</b> ArcoLinux, Endeavour e Manjaro

```zsh
sudo pacman -S telegram-desktop
```

### CentOS e RedHat

```zsh
sudo dnf install -y https://extras.getpagespeed.com/release-el8-latest.rpm
dnf install -y telegram-desktop
```

### Debian, Ubuntu e seus derivados:

```zsh
sudo add-apt-repository ppa:atareao/telegram
sudo apt update
sudo apt install telegram
```

### Fedora e seus derivados

```zsh
sudo dnf install dnf-plugins-core
sudo dnf copr enable rommon/telegram
sudo dnf install telegram-desktop
```

### Gentoo e seus derivados

**Aplica-se:** Bentōō e Funtoo

```zsh
emerge --oneshot net-im/telegram-desktop
```

### openSUSE e seus derivados

```zsh
sudo zypper telegram-desktop
```

### Solus

```zsh
sudo eopkg install telegram
```

### Void Linux

```zsh
xbps-install -S telegram-desktop
```

<h2 id="instalação-em-outros-sistemas">Instalação em outros sistemas {% t global.palavras.operacionais %}</h2>

### FreeBSD

```zsh
sudo pkg install telegram-desktop
```