---
layout: default
title: "Captura de global.palavras.tela"
description: "Ensinará os alunos a tirar uma captura de global.palavras.tela a partir do computador ao invés de do celular"
namespace: captura
permalink: /captura-de-tela.html
permalink_pt_BR: /captura-de-tela.html
permalink_pt_PT: /captura-de-ecrã.html
---

{% if site.lang == "pt_PT" %}
  {% capture link1 %}{{ site.baseurl_root }}/captura-de-tela.html{% endcapture %}
  Você está em português europeu, e caso seja de Brasil, escolha um dos **idiomas:** <a href="{{ link1 }}" alt="Português brasileiro" name="Português brasileiro" title="Português brasileiro">{% t global.pt_br %}</a>
{% elsif site.lang == "pt_BR" %}
  {% capture link2 %}{{ site.baseurl_root }}/pt_PT/captura-de-ecrã.html{% endcapture %}
  Estás em português brasileiro, e caso sejas de Portugal, escolhe um dos **idiomas:** <a href="{{ link2 }}" alt="Português europeu" name="Português europeu" title="Português europeu">{% t global.pt_pt %}</a>
{% endif %}

<h1>Captura de {% t global.palavras.tela %}</h1>

> É importante observar que algumas imagens tiradas do {% t global.palavras.celulares %} não são eficientes e legíveis para a compreensão das pessoas e não claras para as explicações. Há pessoas que entendem as suas imagens, mas ainda se incomodam com as imagens tiradas dos {% t global.palavras.celulares %}, por isto, elas preferem as tiradas do computador. 

Por isso, recomendamos que {% t global.palavras.você %} tire{% t global.palavras.verbo.s %} uma captura cheia ou selecionada a partir do computador. Preparamos os minitutorais divididos para {% t global.palavras.usuários %} de cada sistema {% t global.palavras.operacional %}.

<h2>Conteúdo</h2>

- [Windows 10](#windows-10)
- [Linux](#linux)
  - [Universal independentemente da distribuição](#universal-independentemente-da-distribuição)
  - [Alpine Linux](#alpine-linux)
  - [Arch Linux e seus derivados](#arch-linux-e-seus-derivados)
  - [CentOS, Fedora e RedHat](#centos-fedora-e-redhat)
  - [Debian, Ubuntu e seus derivados](#debian-ubuntu-e-seus-derivados)
  - [Gentoo e seus derivados](#gentoo-e-seus-derivados)
  - [openSUSE e seus derivados](#opensuse-e-seus-derivados)
  - [Solus](#solus)
  - [Void Linux](#void-linux)
- [FreeBSD](#freebsd)

## Windows 10

1. <span class="capitalização">{% t global.palavras.verbo.clique %}</span> no ícone de pesquisa:

    <img src="assets/imagens/capturas/1-captura-windows-10.png" alt="iniciar" />

2. <span class="capitalização">{% t global.palavras.verbo.escreva %}</span> {% t global.símbolo.aspaaberta %}captura{% t global.símbolo.aspafechada %} e {% t global.palavras.verbo.selecione %} o aplicativo {% t global.símbolo.aspaaberta %}Ferramenta de Captura{% t global.símbolo.aspafechada %}:

    <img src="assets/imagens/capturas/2-captura-windows-10.png" alt="aplicativo" />

3. Selecione {% t global.símbolo.aspaaberta %}Experimente Captura e Esboço{% t global.símbolo.aspafechada %}:

    <img src="assets/imagens/capturas/3-captura-windows-10.png" alt="novo-aplicativo" />

4. <span class="capitalização">{% t global.palavras.verbo.clique %}</span> em {% t global.símbolo.aspaaberta %}New{% t global.símbolo.aspafechada %} à esquerda superior:

    <img src="assets/imagens/capturas/4-captura-windows-10.png" alt="nova-captura" />

5. Pode{% t global.palavras.verbo.s %} observar que há 4 opções que pode{% t global.palavras.verbo.s %} escolher:
     - No primeiro ícone, captura retangular;
     - No segundo, captura de forma livre;
     - No terceiro, captura de janela selecionada;
     - No último, captura de {% t global.palavras.tela %} inteira.

    <img src="assets/imagens/capturas/5-captura-windows-10.png" alt="quatro-ícones" />

**Dicas:**

Recomendamos o primeiro ícone para alguns códigos com certos *bugs* se não houver erros e a captura de janela para a janela do editor de texto e para a janela do navegador com previsão do *site* ou para a previsão do jogo. Se {% t global.palavras.verbo.quiser %} privacidade total, não capture{% t global.palavras.verbo.s %} {% t global.frases.telainteira %}, e no caso do navegador, se {% t global.palavras.verbo.quiser %} esconder os {% t global.palavras.seus %} favoritos, {% t global.palavras.verbo.use %} a captura retangular para a área apenas da previsão. 

1. <span class="capitalização">{% t global.palavras.verbo.abra %}</span> o Telegram, {% t global.palavras.verbo.selecione %} o grupo da Dankicode;
2. <span class="capitalização">{% t global.palavras.verbo.clique %}</span> na caixa de mensagem e pressione <kbd>Ctrl</kbd> + <kbd>V</kbd> magicamente. <span class="capitalização">{% t global.palavras.verbo.envie %}</span>. Pronto!

Voilà! &#x1F600;

## Linux

Cada distribuição tem um próprio aplicativo de captura de {% t global.palavras.tela %}, mas recomendamos fortemente o Spectacle, que é um aplicativo do KDE, que pode ser instalado em qualquer ambiente gráfico. 

Primeiramente {% t global.palavras.verbo.instale %} o aplicativo em seguida:

### Universal independentemente da distribuição

**Aplica-se:** CentOS, Mageia, Porteus, RedHat e Slackware.

<span class="capitalização">{% t global.palavras.verbo.observe %}</span> que se não {% t global.palavras.verbo.tiver %} Flatpak ou Snap, é necessário instalá-los.

<dl>
  <dt>Flatpak (recomendado)</dt>
  Não há versão disponível de Spectacle no Flatpak

  </dd>

  <dt>Snapcraft (não recomendado)</dt>
  <dd><span class="capitalização">{% t global.palavras.verbo.abra %}</span> o terminal:

  ```zsh
  sudo snap install spectacle --candidate
  ```

  </dd>
</dl>

### Alpine Linux

```zsh
apk add spectacle
```

### Arch Linux e seus derivados

<b>Derivados:</b> ArcoLinux, Endeavour e Manjaro

```zsh
sudo pacman -S spectacle
```

### CentOS, Fedora e RedHat

```zsh
sudo dnf install -y spectacle
```

### Debian, Ubuntu e seus derivados

```zsh
sudo apt install spectacle
```

### Gentoo e seus derivados

**Aplica-se:** Bentōō e Funtoo

```zsh
emerge --oneshot kde-apps/spectacle
```

### openSUSE e seus derivados

```zsh
sudo zypper spectacle
```

### Solus

```zsh
sudo eopkg spectacle
```

### Void Linux

```zsh
xbps-install -S spectacle
```

## FreeBSD

1. Primeiramente {% t global.palavras.verbo.execute %} o comando para atualização dos *ports* e depois {% t global.palavras.verbo.execute %} o comando para atualizar:

   ```zsh
   $ portsnap fetch extract
   $ pkg update
   ```

2. Para que o aplicativo Spectacle funcione:

   ```zsh
   $ pkg install xorg
   $ pkg install x11/kde5
   ```

Pronto. <span class="capitalização">{% t global.palavras.verbo.ache %}</span> o Spectacle na lista de aplicativos.

