---
layout: default
title: pages.placas-de-vídeo.title
description: pages.placas-de-vídeo.description
permalink: /placas-de-vídeo.html
---

{% if site.lang == "pt_PT" %}
  {% capture link1 %}{{ site.baseurl_root }}{{ page.url }}{% endcapture %}
  Você está em português europeu, e caso seja de Brasil, escolha um dos **idiomas:** <a href="{{ link1 }}" alt="Português brasileiro" name="Português brasileiro" title="Português brasileiro">{% t global.pt_br %}</a>
{% elsif site.lang == "pt_BR" %}
  {% capture link2 %}{{ site.baseurl_root }}/pt_PT{{ page.url }}{% endcapture %}
  Estás em português brasileiro, e caso sejas de Portugal, escolhe um dos **idiomas:** <a href="{{ link2 }}" alt="Português europeu" name="Português europeu" title="Português europeu">{% t global.pt_pt %}</a>
{% endif %}

# Instalar as placas de vídeo antes de gravar um vídeo <!-- omit in toc -->

## Conteúdo <!-- omit in toc -->

- [Windows](#windows)
- [Linux](#linux)
  - [AMD](#amd)
    - [Alpine Linux](#alpine-linux1)
      - [Para as placas de vídeo antigas da marca AMD](#para-as-placas-de-vídeo-antigas-da-marca-amd)
      - [Para as placas de vídeo novas da marca AMD](#para-as-placas-de-vídeo-novas-da-marca-amd)
    - [Arch Linux](#arch-linux)
      - [Para as placas de vídeo antigas da marca AMD](#para-as-placas-de-vídeo-antigas-da-marca-amd-1)
      - [Para as placas de vídeo novas da marca AMD](#para-as-placas-de-vídeo-novas-da-marca-amd-1)
    - [CentOS, Fedora e RedHat](#centos-fedora-e-redhat)
      - [Para as placas de vídeo antigas da marca AMD](#para-as-placas-de-vídeo-antigas-da-marca-amd-2)
      - [Para as placas de vídeo novas da marca AMD](#para-as-placas-de-vídeo-novas-da-marca-amd2)
    - [Debian e derivados](#debian-e-derivados)
      - [Para as placas de vídeo antigas da marca AMD](#para-as-placas-de-vídeo-antigas-da-marca-amd-3)
      - [Para as placas de vídeo novas da marca AMD](#para-as-placas-de-vídeo-novas-da-marca-amd-2)
    - [Gentoo e seus derivados](#gentoo-e-seus-derivados)
    - [Ubuntu 16.04 e superior](#ubuntu-1604-e-superior)
      - [Para todas as placas de vídeos antigas e novas da marca AMD](#para-todas-as-placas-de-vídeos-antigas-e-novas-da-marca-amd)
- [Referências](#referências)

## Windows

Windows reconhece e instala automaticamente as placas de vídeo das marcas AMD e NVIDIA. Para fazer isto:

1. <span class="capitalização">{% t global.palavras.verbo.clique %}</span> no ícone de logotipo da Microsoft e então no ícone de engrenagem. 
2. Na figura 1, {% t global.palavras.verbo.selecione %} {% t global.símbolo.aspaaberta %}Atualização e Segurança{% t global.símbolo.aspafechada %}:

   <figure>
     <figcaption>Figura 1</figcaption>

     ![Atualização e Segurança](assets/imagens/capturas/22-captura-placas-de-v%C3%ADdeo.png)

   </figure>

3. <span class="capitalização">{% t global.palavras.verbo.clique %}</span> em {% t global.símbolo.aspaaberta %}Verificar se há atualizações{% t global.símbolo.aspafechada %}. Logo instalar e reiniciar.

## Linux

### AMD

#### Alpine Linux[^1]

##### Para as placas de vídeo antigas da marca AMD

Aplica-se às placas:

- AMD Raedon™ X1000;
- AMD Raedon™ HD 1000, 2000, 3000, 4000, 5000 e 6000.

<br>

1. <span class="capitalização">{% t global.palavras.verbo.instale %}</span> os *drivers*:

   ```zsh
   apk add xf86-video-ati
   ```

2. <span class="capitalização">{% t global.palavras.verbo.habilite %}</span> o KMS no *boot*:

   ```zsh
   $ echo radeon >> /etc/modules
   $ echo fbcon >> /etc/modules
   ```

3. <span class="capitalização">{% t global.palavras.verbo.instale %}</span> o pacote `mkinitfs`:

   ```zsh
   apk add mkinitfs
   ```

4. <span class="capitalização">{% t global.palavras.verbo.habilite %}</span> a função `kms` na configuração de `mkinitfs`, adicionando-a para a váriavel de função, por exemplo:

   Entrar com `nano`:

   ```zsh
   sudo nano /etc/mkinitfs/mkinitfs.conf
   ```

   Então:

   ```zsh
   features="keymap cryptsetup kms ata base ide scsi usb virtio ext4"
   ```

5. <span class="capitalização">{% t global.palavras.verbo.execute %}</span> o `mkinitfs`.
6. <span class="capitalização">{% t global.palavras.verbo.reinicie %}</span> para testar a configuração.

##### Para as placas de vídeo novas da marca AMD

Aplica-se às placas:

- AMD Raedon™ HD 7000;
- AMD Raedon™ RX 200, 300, 400 e 500;
- AMD Raedon™ RX Vega;
- AMD Raedon™ RX 5000 (Navi).

<br>

1. <span class="capitalização">{% t global.palavras.verbo.instale %}</span> os *drivers*:

   ```zsh
   apk add xf86-video-amdgpu
   ```

2. <span class="capitalização">{% t global.palavras.verbo.habilite %}</span> o KMS no *boot*:

   ```zsh
   $ echo amdgpu >> /etc/modules
   $ echo fbcon >> /etc/modules
   ```

3. <span class="capitalização">{% t global.palavras.verbo.instale %}</span> o pacote `mkinitfs`:

   ```zsh
   apk add mkinitfs
   ```

4. <span class="capitalização">{% t global.palavras.verbo.habilite %}</span> a função `kms` na configuração de `mkinitfs`, adicionando-a para a váriavel de função, por exemplo:

   Entrar com `nano`:

   ```zsh
   sudo nano /etc/mkinitfs/mkinitfs.conf
   ```

   Então:

   ```zsh
   features="keymap cryptsetup kms ata base ide scsi usb virtio ext4"
   ```

5. <span class="capitalização">{% t global.palavras.verbo.execute %}</span> o `mkinitfs`.
6. <span class="capitalização">{% t global.palavras.verbo.reinicie %}</span> para testar a configuração.

#### Arch Linux

##### Para as placas de vídeo antigas da marca AMD

Aplica-se:

- AMD Raedon™ X1000;
- AMD Raedon™ HD 1000, 2000, 3000, 4000, 5000 e 6000.

<br>

1. <span class="capitalização">{% t global.palavras.verbo.instale %}</span> os *drivers*:

   ```zsh
   sudo pacman -S xf86-video-ati 
   ```

2. <span class="capitalização">{% t global.palavras.verbo.reinicie %}</span> o computador. 

##### Para as placas de vídeo novas da marca AMD

Aplica-se às placas:

- AMD Raedon™ HD 7000;
- AMD Raedon™ RX 200, 300, 400 e 500;
- AMD Raedon™ RX Vega;
- AMD Raedon™ RX 5000 (Navi).

<br>

1. <span class="capitalização">{% t global.palavras.verbo.instale %}</span> os *drivers*:

   ```zsh
   sudo pacman -S xf86-video-amdgpu
   ```

2. <span class="capitalização">{% t global.palavras.verbo.reinicie %}</span> o computador. 

#### CentOS, Fedora e RedHat

##### Para as placas de vídeo antigas da marca AMD

Aplica-se:

- AMD Raedon™ X1000;
- AMD Raedon™ HD 1000, 2000, 3000, 4000, 5000 e 6000.

<br>

1. <span class="capitalização">{% t global.palavras.verbo.instale %}</span> os *drivers*:

   ```zsh
   sudo dnf install xorg-x11-drv-ati
   ```

2. <span class="capitalização">{% t global.palavras.verbo.instale %}</span> o GRUB com as novas configurações:

   - (U)EFI:

     ```zsh
     $ sudo grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
     ```

   - BIOS:
  
     ```zsh
     $ sudo grub2-mkconfig -o /boot/grub2/grub.cfg
     ```

3. <span class="capitalização">{% t global.palavras.verbo.reinicie %}</span> o computador. 

##### Para as placas de vídeo novas da marca AMD[^2]

Aplica-se às placas:

- AMD Raedon™ HD 7000;
- AMD Raedon™ RX 200, 300, 400 e 500;
- AMD Raedon™ RX Vega;
- AMD Raedon™ RX 5000 (Navi).

<br>

1. <span class="capitalização">{% t global.palavras.verbo.instale %}</span> os *drivers*:

   ```zsh
   sudo dnf install xorg-x11-drv-amdgpu
   ```

2. Entra com `nano` ou outro editor de texto favorito no {% t global.palavras.arquivo %} `/etc/default/grub` e {% t global.palavras.verbo.modifique %}:

   - Southern Islands (AMD Raedon™ HD 7000):

     ```zsh
     GRUB_CMDLINE_LINUX="rhgb quiet splash amdgpu.dc=0 radeon.si_support=0 amdgpu.si_support=1"
     ```

   - Sea Islands (AMD Raedon™ RX 200), Volcanic Islands (AMD Raedon™ RX 300), Polaris (AMD Raedon™ RX 400 e 500), Vega e Navi:

     ```zsh
     GRUB_CMDLINE_LINUX="rhgb quiet splash amdgpu.dc=0 radeon.cik_support=0 amdgpu.cik_support=1"
     ```

3. <span class="capitalização">{% t global.palavras.verbo.instale %}</span> o GRUB com as novas configurações:

   - (U)EFI:

     ```zsh
     $ sudo grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
     ```

   - BIOS:
  
     ```zsh
     $ sudo grub2-mkconfig -o /boot/grub2/grub.cfg
     ```

4. <span class="capitalização">{% t global.palavras.verbo.reinicie %}</span> o computador. 

#### Debian e derivados

##### Para as placas de vídeo antigas da marca AMD

Aplica-se:

- AMD Raedon™ X1000;
- AMD Raedon™ HD 1000, 2000, 3000, 4000, 5000 e 6000.

<br>

1. <span class="capitalização">{% t global.palavras.verbo.instale %}</span> os *drivers*:

   ```zsh
   sudo apt-get install firmware-linux-nonfree libgl1-mesa-dri xserver-xorg-video-ati 
   ```

2. <span class="capitalização">{% t global.palavras.verbo.reinicie %}</span> o computador.

##### Para as placas de vídeo novas da marca AMD

Aplica-se:

- AMD Raedon™ HD 7000;
- AMD Raedon™ RX 200, 300, 400 e 500;
- AMD Raedon™ RX Vega;
- AMD Raedon™ RX 5000 (Navi).

<br>

1. <span class="capitalização">{% t global.palavras.verbo.instale %}</span> os *drivers*:

   ```zsh
   sudo apt-get install firmware-linux-nonfree libgl1-mesa-dri xserver-xorg-video-amdgpu
   ```

2. <span class="capitalização">{% t global.palavras.verbo.reinicie %}</span> o computador.

#### Gentoo e seus derivados

**Aplica-se:** Bentōō e Funtoo

##### Para as placas de vídeo antigas da marca AMD

Aplica-se:

- AMD Raedon™ X1000;
- AMD Raedon™ HD 1000, 2000, 3000, 4000, 5000 e 6000.

<br>

1. <span class="capitalização">{% t global.palavras.verbo.abra %}</span> o terminal e {% t global.palavras.verbo.execute %} o comando:

   ```zsh
   emerge --oneshot x11-drivers/xf86-video-ati
   ```

#### Ubuntu 16.04 e superior

##### Para todas as placas de vídeos antigas e novas da marca AMD

Aplica-se:

- AMD Raedon™ X1000;
- AMD Raedon™ HD 1000, 2000, 3000, 4000, 5000 e 6000.
- AMD Raedon™ HD 7000;
- AMD Raedon™ RX 200, 300, 400 e 500;
- AMD Raedon™ RX Vega;
- AMD Raedon™ RX 5000 (Navi).

<br>

1. <span class="capitalização">{% t global.palavras.verbo.abra %}</span> o terminal e {% t global.palavras.verbo.execute %} o comando:

   ```zsh
   $ sudo apt update
   $ sudo apt install mesa-utils mesa-va-drivers mesa-vdpau-drivers ubuntu-drivers-common xserver-xorg-core xserver-xorg xserver-xorg-input-all xserver-xorg-legacy xserver-xorg-video-all xserver-xorg-video-amdgpu xserver-xorg-video-ati xserver-xorg-video-radeon
   ```

2. <span class="capitalização">{% t global.palavras.verbo.reinicie %}</span> o computador.

## Referências

[^1]: <a href="https://wiki.alpinelinux.org/wiki/Radeon_Video" rel="external">{% t global.símbolo.aspaaberta %}Radeon Video{% t global.símbolo.aspafechada %}</a>. (22 de dezembro de 2019). *Alpine Linux Wiki*. Consultado em 29 de março de 2020. 
[^2]: Schott, A. (26 de março de 2020). <a href="https://schotty.com/EL_And_Fedora/amdgpu_Plus_Fedora/" rel="external">{% t global.símbolo.aspaaberta %}AMD Raedon™ Setup on Fedora Linux{% t global.símbolo.aspafechada %}</a>. *Schotty*. Consultado em 29 de março de 2020.