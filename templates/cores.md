### Cores derivadas da Dankicode

- #F54C6F
- #FE5A4E
- #D22338
- #9975e6
- #5E3EA1
- #3E2A68
- #DCD4E5
- #F5F5F5
- #1BF2A5
- #5FB382
- #FDCD0A
- #FE9E01
- #F6F3EE
- #23282B

### Cores originais do Paraíso Escuro em ordem

- #06b6ef
- #2f1e2e
- #48b685
- #4f424c
- #5bc4bf
- #776e71
- #815ba4
- #e7e9db
- #ef6155
- #f99b15
- #fec418

serão subsitutidas pelas cores em ordem

- #1BF2A5
- #23282B
- #5FB382
- #313131
- #DCD4E5
- #B3ADBA
- #FD3660
- #9975e6
- #F3746B
- #FE9E01
- #FDCD0A
